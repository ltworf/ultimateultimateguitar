.PHONY: mypy
mypy:
	MYPYPATH=stubs mypy --config-file mypy.conf ultimateultimateguitar

.PHONY: dist
dist:
	cd ..; tar -czvvf ultimateultimateguitar.tar.gz \
		ultimateultimateguitar/CHANGELOG \
		ultimateultimateguitar/LICENSE \
		ultimateultimateguitar/Makefile \
		ultimateultimateguitar/mypy.conf \
		ultimateultimateguitar/README.md \
		ultimateultimateguitar/requirements.txt \
		ultimateultimateguitar/stubs/ \
		ultimateultimateguitar/man/ \
		ultimateultimateguitar/*.py \
		ultimateultimateguitar/ultimateultimateguitar/*.py
	mv ../ultimateultimateguitar.tar.gz ultimateultimateguitar_`head -1 CHANGELOG`.orig.tar.gz
	gpg --detach-sign -a *.orig.tar.gz

deb-pkg: dist
	mv ultimateultimateguitar_`head -1 CHANGELOG`.orig.tar.gz* /tmp
	cd /tmp; tar -xf ultimateultimateguitar*.orig.tar.gz
	cp -r debian /tmp/ultimateultimateguitar/
	cd /tmp/ultimateultimateguitar/; dpkg-buildpackage --changes-option=-S
	install -d deb-pkg
	mv /tmp/ultimateultimateguitar_* deb-pkg
	$(RM) -r /tmp/ultimateultimateguitar
	lintian --pedantic -E --color auto -i -I deb-pkg/*.changes deb-pkg/*.deb

.PHONY: clean
clean:
	$(RM) -r deb-pkg
	$(RM) -r dist
	$(RM) -r ultimateultimateguitar.egg-info
	$(RM) -r build

.PHONY: upload
upload:
	$(RM) -r dist
	./setup.py sdist
	./setup.py bdist_wheel
	twine upload --username __token__ --password `cat .token` dist/*
